import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { exit } from 'process';
import { Observable, timer } from 'rxjs';

const counter = timer(0, 2000);

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit
{
  public form: FormGroup;
  public genericError: string = null;
  public exist = false;
  public not_exist = false;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void
  {
    this.form = this.fb.group({
      test_id: [null, [Validators.required]]
    });
  }

  public checkID(form: FormGroup)
  {
    this.exist = false;
    this.not_exist = false;
    if (form.value['test_id'].toString() === '25206577')
    {
      this.exist = true;
    } else
    {
      this.not_exist = true;
      setInterval(() =>
      {
        this.not_exist = false;
        this.form = this.fb.group({
          test_id: [null, [Validators.required]]
        });
      }, 6000);
    }
  }
}
