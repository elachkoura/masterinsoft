export const environment = {
  production: true,
  baseUrlGames: 'https://stage.whgstage.com/front-end-test/games.php',
  baseUrlJackpots: 'https://stage.whgstage.com/front-end-test/jackpots.php'
};
